﻿namespace Otus.Teaching.Concurrency.Import.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
