using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly ApplicationDbContext _ctx;

        public CustomerRepository(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public void AddCustomer(Customer customer)
        {
            _ctx.Customers.Add(customer);
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }
    }
}