﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _ctx;

        public UserRepository(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task AddAsync(User user)
        {
           var result =  await _ctx.Users.AddAsync(user);
        }

        public async Task<bool> Exist(int id)
        {
            return await _ctx.Users.AnyAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _ctx.Users.ToListAsync();
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await _ctx.Users.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _ctx.SaveChangesAsync();
        }
    }
}
