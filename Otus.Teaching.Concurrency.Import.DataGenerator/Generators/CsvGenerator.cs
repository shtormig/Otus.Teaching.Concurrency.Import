﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using ServiceStack.Text;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : BaseGenerator
    {
        public CsvGenerator(string fileName, int dataCount)
            : base(fileName, dataCount)
        {

        }

        public override void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.CreateText(_fileName);

            var customersList = new CustomersList()
            {
                Customers = customers
            };

            CsvSerializer.SerializeToWriter<CustomersList>(customersList, stream);
        }
    }
}
